package com.vergth.game1;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class MainActivity extends Activity {

    private Integer correctNumber=0; //περιέχει τη τιμή της λύσης
    private Integer correctsAnswers=0; //περιέχει τον αριθμό των σωστών απαντήσεων
    private Integer wrongsAnswers=0; //περιέχει τον αριθμό των λάθος απαντήσεων
    private Integer addmul=0; //βοηθητική μεταβλητή επιλογής μεταξεί πρόσθεσης και πολαπλασιασμού

    //δήλωση κουμπιών (buttons)
    Button b1;
    Button b2;
    Button b3;
    Button b4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //εκκίνηση του αλγορίθμου του παιχνιδιού quiz
        quiz_function();

        //δήλωση της συναρτήσεως που θα καλείται στο πάτημα του κάθε button
        b1 = (Button) findViewById(R.id.button1);
        b1.setOnClickListener(myhandler1);

        b2 = (Button) findViewById(R.id.button2);
        b2.setOnClickListener(myhandler2);

        b3 = (Button) findViewById(R.id.button3);
        b3.setOnClickListener(myhandler3);

        b4 = (Button) findViewById(R.id.button4);
        b4.setOnClickListener(myhandler4);

    }

    //εδώ περιλαμβάνονται οι εντολές που πρέπει να γίνουν όταν ο χρήστης πατάει το πρώτο button
    View.OnClickListener myhandler1 = new View.OnClickListener() {
        public void onClick(View v) {
            //λαμβάνουμε τη τιμή που περιέχεται στο button
            String buttonText = b1.getText().toString();
            //έλεγχος αν η απάντηση ήταν σωστή
            if(correctNumber==Integer.valueOf(buttonText)){
                //αύξηση κατά ένα των σωστών απαντήσεων
                correctsAnswers=correctsAnswers+1;
                //εμφάνιση μηνύματος πλήθους σωστών απαντήσεων
                Toast.makeText(MainActivity.this, "Correct Answer Number: "+correctsAnswers.toString(),
                        Toast.LENGTH_LONG).show();
                //περίπτωση που η απάντηση δεν είναι σωστή
            }else{
                //αύξηση κατά ένα των λάθος απαντήσεων
                wrongsAnswers=wrongsAnswers+1;
                //εμφάνιση μηνύματος πλήθους λάθος απαντήσεων
                Toast.makeText(MainActivity.this, "Wrong Answer Number: "+wrongsAnswers.toString(),
                        Toast.LENGTH_LONG).show();
            }
            //στο τέλος των εντολών που γίνονται μετά το πάτημα του button πρέπει να ξανακαλέσουμε
            //τη συνάρτηση του παιχνιδιού ώστε να πάμε στην επόμενη ερώτηση του παιχνιδιού
            quiz_function();
        }
    };

    //όπως και πριν έτσι και εδώ περιλαμβάνουμε τις ενέργειες που πρέπει να γίνουν όταν θα πατηθεί
    //το δεύτερο button
    View.OnClickListener myhandler2 = new View.OnClickListener() {
        public void onClick(View v) {
            String buttonText = b2.getText().toString();
            if(correctNumber==Integer.valueOf(buttonText)){
                correctsAnswers=correctsAnswers+1;
                Toast.makeText(MainActivity.this, "Correct Answer Num: "+correctsAnswers.toString(),
                        Toast.LENGTH_LONG).show();
            }else{
                wrongsAnswers=wrongsAnswers+1;
                Toast.makeText(MainActivity.this, "Wrong Answer Num: "+wrongsAnswers.toString(),
                        Toast.LENGTH_LONG).show();
            }
            quiz_function();
        }
    };

    //παρόμια για τα button 3 και 4 στη συνέχεια
    View.OnClickListener myhandler3 = new View.OnClickListener() {
        public void onClick(View v) {
            String buttonText = b3.getText().toString();
            if(correctNumber==Integer.valueOf(buttonText)){
                correctsAnswers=correctsAnswers+1;
                Toast.makeText(MainActivity.this, "Correct Answer Num: "+correctsAnswers.toString(),
                        Toast.LENGTH_LONG).show();
            }else{
                wrongsAnswers=wrongsAnswers+1;
                Toast.makeText(MainActivity.this, "Wrong Answer Num: "+wrongsAnswers.toString(),
                        Toast.LENGTH_LONG).show();
            }
            quiz_function();
        }
    };

    View.OnClickListener myhandler4 = new View.OnClickListener() {
        public void onClick(View v) {
            String buttonText = b4.getText().toString();
            if(correctNumber==Integer.valueOf(buttonText)){
                correctsAnswers=correctsAnswers+1;
                Toast.makeText(MainActivity.this, "Correct Answer Num: "+correctsAnswers.toString(),
                        Toast.LENGTH_LONG).show();
            }else{
                wrongsAnswers=wrongsAnswers+1;
                Toast.makeText(MainActivity.this, "Wrong Answer Num: "+wrongsAnswers.toString(),
                        Toast.LENGTH_LONG).show();
            }
            quiz_function();
        }
    };

    //ο κώδικας του παιχνιδιού
    public void quiz_function() {

        //δήλωση των μεταβλητών που θα περιλαμβάνουν τα αποτελέσματα των πράξεων πρόσθεσης και πολλαπλασιασμού
        int sum=0,mul=0;

        //δήλωση χρήσης του TextView (περιλαμβάνει το κείμενο το οποίο θα περιέχει την εξίσωση που θα πρέπει να λύσει ο χρήστης)
        TextView myAwesomeTextView = (TextView) findViewById(R.id.textView);

        //δημιουργούμε 4 τυχαίους αριθμούς
        int randomNum1 = 1 + (int) (Math.random() * 3); //ορίζουμε οτι το εύρος της τιμης του randomNum1 θα είναι απο 1 έως 3
        int randomNum2 = 5 + (int) (Math.random() * 7); //ορίζουμε οτι το εύρος της τιμης του randomNum2 θα είναι απο 5 έως 7
        int randomNum3 = 9 + (int) (Math.random() * 12); //ορίζουμε οτι το εύρος της τιμης του randomNum3 θα είναι απο 9 έως 12
        int randomNum4 = 15 + (int) (Math.random() * 21); //ορίζουμε οτι το εύρος της τιμης του randomNum4 θα είναι απο 15 έως 21

        //επιλογή πρόσθεσης ή πολλαπλασιασμού
        if(addmul==0){
            //περίπτωση πρόσθεσης
            addmul=1; //ορίζουμε το addmul=1 ώστε την επόμενη φορά που θα τρέξει η συνάρτηση του παιχνιδιού να μην γίνει πρόσθεση αλλά πολαπλασιασμός
            sum = randomNum1 + randomNum2;
            //εμφανίζουμε την εξίσωση πρόσθεσης στο κείμενο του TextView
            myAwesomeTextView.setText(randomNum1 + "+?" + "=" + sum);
        }else{
            //περίπτωση πολλαπλασιασμού
            addmul=0;
            mul = randomNum1 * randomNum2;
            //εμφανίζουμε την εξίσωση πολλαπλασιασμού στο κείμενο του TextView
            myAwesomeTextView.setText(randomNum1 + "*?" + "=" + mul);
        }

        //καταχώρηση της σωστής τιμής στη μεταβλητή correctNumber ώστε να μπορέσουμε να τη χρησιμοποιήσουμε
        //όταν ο χρήστης θα πατήσει κάποιο button και θα εισάγει με αυτό τον τρόπο την επιλογή του
        correctNumber = randomNum2;

        //εισαγωγή των τυχαίων τιμών randomNum1,randomNum2,randomNum3,randomNum4 σε λίστα
        List<Integer> solution = new ArrayList<>();
        solution.add(randomNum1);
        solution.add(randomNum2);
        solution.add(randomNum3);
        solution.add(randomNum4);

        //ανακάτεμα των τιμών της λίστας ώστε να εισαχθούν με τυχαίο τρόπο στα buttons
        Collections.shuffle(solution);

        //εισαγωγή των τιμών που περιέχονται στη λίστα στα buttons
        Button mButton1 = (Button) findViewById(R.id.button1);
        mButton1.setText(Integer.toString(solution.get(0)));

        Button mButton2 = (Button) findViewById(R.id.button2);
        mButton2.setText(Integer.toString(solution.get(1)));

        Button mButton3 = (Button) findViewById(R.id.button3);
        mButton3.setText(Integer.toString(solution.get(2)));

        Button mButton4 = (Button) findViewById(R.id.button4);
        mButton4.setText(Integer.toString(solution.get(3)));
    }
}
